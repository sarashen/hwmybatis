package com.tgl.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.mvc.dao.EmployeeMyBatisDao;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.Util;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Service
public class EmployeeMybatisService {

	@Autowired
	private EmployeeMyBatisDao employeeMyBatisDao;
	
	@ApiResponses(value = {@ApiResponse(code=200,message="查詢成功")})
	public Employee inquiryId(long employeeId) {
		Employee rs = employeeMyBatisDao.inquiryId(employeeId);
		if (rs == null) {
			return null;
		}
		Util util = new Util();
		rs.setCname(util.maskutil(rs.getCname()));
		return rs;
	} // end inquiry

	@ApiResponses(value = {@ApiResponse(code=201,message="新增成功")})
	public long insert(Employee employee) {

		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		employeeMyBatisDao.insert(employee);
		return employee.getId();
	} // end insert

	public int update(Employee employee) {

		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		return employeeMyBatisDao.update(employee);
	} // end update

	
	@ApiResponses(value = {@ApiResponse(code=200,message="刪除成功", response = Void.class),
						   @ApiResponse(code=404,message="找不到資料")
	})
	public boolean delete(long employeeId) {
		return employeeMyBatisDao.delete(employeeId);
	} // end delete

} // end class
