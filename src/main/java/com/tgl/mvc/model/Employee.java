package com.tgl.mvc.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("員工基本資料")
public class Employee {

	@ApiModelProperty(value = "員工编號", required = true)
	private long id;

	@ApiModelProperty(value = "身高", required = true)
	@Min(value = 100, message = "身高需大於100 公分 ")
	@Max(value = 210, message = "身高需小於210 公分 ")
	private float high;

	@ApiModelProperty(value = "體重", required = true)
	@Min(value = 30, message = "體重需大於30 公斤 ")
	@Max(value = 120, message = "體重需小於120 公斤 ")
	private float weight;

	@ApiModelProperty(value = "英文姓名", required = true)
	@NotNull(message = "姓名不可空白")
	private String ename;

	@ApiModelProperty(value = "中文姓名", required = true)
	@NotBlank(message = "姓名不可空白")
	private String cname;

	@ApiModelProperty(value = "分機", required = false)
/*	@Pattern(regexp = "[0-9] {4}", message = "需輸入分機") */
	@NotEmpty(message = "請輸入分機") 
	private String ext;

	@ApiModelProperty(value = "e-mail", required = false)
	@Email(message = "e-mail 有誤")
	private String email;

	@ApiModelProperty(value = "BMI", required = false)
	private float bmi;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getHigh() {
		return high;
	}

	public void setHigh(float high) {
		this.high = high;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBmi() {
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", high=" + high + ", weight=" + weight + ", ename=" + ename + ", cname=" + cname
				+ ", ext=" + ext + ", email=" + email + ", bmi=" + bmi + "]";
	}

}
