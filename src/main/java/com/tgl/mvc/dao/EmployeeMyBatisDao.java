package com.tgl.mvc.dao;

import com.tgl.mvc.model.Employee;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeMyBatisDao {

	Employee inquiryId(long id);

	long insert(Employee employee);

	int  update(Employee employee);

	boolean delete(long id);
}
