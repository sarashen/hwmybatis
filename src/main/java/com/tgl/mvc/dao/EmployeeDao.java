package com.tgl.mvc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tgl.mvc.mapper.EmployeeRowMapper;
import com.tgl.mvc.model.Employee;

@Repository
public class EmployeeDao {

	private static final String SELECT = "SELECT id,high,weight,cname,ename,email,ext,bmi FROM employee";

	private static final String INSERT = "INSERT INTO employee (high,weight,ename,cname,email,ext,bmi)"
			+ " VALUE(:high, :weight, :ename, :cname, :email, :ext, :bmi )";

	private static final String UPDATE = "UPDATE employee SET high=:high, weight=:weight, cname=:cname, ename=:ename, ext=:ext, bmi=:bmi, email=:email WHERE id=:id ";

	private static final String DELETE = "DELETE FROM employee WHERE id=:id ";

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Cacheable(value = "empInquiryId", key = "#employeeId")
	public Employee inquiryId(long employeeId) {
		try {
			return namedParameterJdbcTemplate.queryForObject(SELECT + " WHERE id=:id",
					new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			System.out.println("EmployeeDao inquiry failed, id:" + employeeId + e);
		}
		return null;
	} // end inquiryId

	public long insert(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		System.out.println("keyHolder" + keyHolder);
		namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee), keyHolder);
		return keyHolder.getKey().longValue();
	}

	@CachePut(value = "empInquiryId", key = "#employee.id")
	public Employee update(Employee employee) {
		namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(employee));
		return employee;
	}

	@CacheEvict(value = "empInquiryId", key = "#employeeId")
	public boolean delete(long employeeId) {
		return namedParameterJdbcTemplate.update(DELETE, new MapSqlParameterSource("id", employeeId)) > 0;
	}

} // end dao
